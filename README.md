# README #

CRUD приложение с использованием JDBC api и JDBC Template

### Настройка ###

1)

Настройки БД для PersonDAOTemplate находятся в файле SpringConfig.java в методе dataSource()

Настройки БД для PersonDAOApi находятся в файле PersonDAOApi.java

2)

Изменение методологии происходит в файле PeopleController.java

В строке 22 @Qualifier("personDAOApi") меняется на @Qualifier("personDAOTemplate")

3)

БД Postgresql

Приложение PGAdmin 4

Для корректной работы необходимо создать БД (id, name, age, email),

где id not null, type: identity