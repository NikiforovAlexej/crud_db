package springcourse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import springcourse.models.Person;

/*PersonMapper() отображает строки из таблицы в нашей сущности
его замена - BeanPropertyRowMapper<>(Person.class), который автоматически присваивает значения выбранному классу*/
public class PersonMapper implements RowMapper<Person>{

	@Override
	public Person mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Person person = new Person();

        person.setId(resultSet.getInt("id"));
        person.setName(resultSet.getString("name"));
        person.setEmail(resultSet.getString("email"));
        person.setAge(resultSet.getInt("age"));
		return person;
	}
	
}
