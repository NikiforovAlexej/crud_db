package springcourse.dao;

import java.util.List;

import springcourse.models.Person;

public interface PersonDAO {

	public List<Person> index();
	
	public Person show(int i);
	
	public void save(Person p);
	
	public void update(int i, Person p);
	
	public void delete(int i);
}
