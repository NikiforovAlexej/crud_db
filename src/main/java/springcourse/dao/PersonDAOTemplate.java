package springcourse.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import springcourse.models.Person;

@Component
public class PersonDAOTemplate implements PersonDAO{
	
	private final JdbcTemplate jdbcTemplate;	
	
	@Autowired
    public PersonDAOTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public List<Person> index() {
	    return jdbcTemplate.query("SELECT * FROM Person", new BeanPropertyRowMapper<>(Person.class));
	}
	
	@Override	
	public Person show(int id) {
	    return jdbcTemplate.query("SELECT * FROM Person WHERE id=?", new BeanPropertyRowMapper<>(Person.class), new Object[]{id}).stream().findAny().orElse(null);
	}
	
	@Override	
	public void save(Person person) {    
		jdbcTemplate.update("INSERT INTO Person(name, age, email) VALUES(?, ?, ?)", person.getName(), person.getAge(), person.getEmail());
	}
	
	@Override	
	public void update(int id, Person updatedPerson) {
		jdbcTemplate.update("UPDATE Person SET name=?, age=?, email=? WHERE id=?", updatedPerson.getName(),
	            			 updatedPerson.getAge(), updatedPerson.getEmail(), id);
	}
	
	@Override	
	public void delete(int id) {
		jdbcTemplate.update("DELETE FROM Person WHERE id=?", id);
		
	}
}